---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of December.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### December 2022

### Advanced CI/CD
#### December 12th, 2022 at 11:00AM-12:00PM Eastern Time/4:00-5:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_rcWQL9pORZC5Ifv3p2BmuQ)

### DevSecOps/Compliance
#### December 14th, 2022 at 11:00AM-12:00PM Eastern Time/4:00-5:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_VCSIo1tkRvWMZqASVVsONg)

### Holistic Approach to Securing the Development Lifecycle
#### December 19th, 2022 at 11:00AM-12:00PM Eastern Time/4:00-5:00 PM UTC

Join this one-hour webinar to gain a better understanding of how to successfully shift security left to find and fix security flaws during development, and to do so more easily and with greater visibility and control than typical approaches can provide.

In this session, we will go over the security and compliance features that are available with your GitLab Ultimate account, including:
- How to achieve comprehensive security scanning without introducing a multitude of tools and systems that expand your attack surface.
- How to secure and protect your cloud-native applications and IaC environments within existing DevOps workflows.
- How to use a single source of truth to improve collaboration between development and security.
- How to manage all of your software vulnerabilities in one place.
- How to set up compliance pipelines to provide consistent guardrails for developers, and ensure separation of duties
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_5CjkEj2BSECyiV5scA9Gkw)

### Janurary 2023

### Intro to GitLab
#### January 4th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_rOGjTmoASI2GBtZ-7CFbsw)

### Intro to CI/CD
#### January 10th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Tra-s19dTemUiFQOqmoJJg)

### Advanced CI/CD
#### January 18th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_hTiT51FvTkiyb2nv9JL2Sw)

### Getting Started with DevOps Metrics
#### January 24th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Aj--MN3ESgi6ESabnaarPw)

### DevSecOps/Compliance
#### January 31st, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_EctIA046QJydBCNo4kJK3Q)



