---
layout: handbook-page-toc
title: Service Desk Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Service Desk Single-Engineer Group

The Service Desk SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

### Vision

Our goal is to provide a complete, yet lightweight and customizable customer support solution that seamlessly integrates with the GitLab ecosystem and brings customers, support staff and developers closer together.

### Mission

Make Service Desk useful for professional support teams so they efficiently and effectively work through their support issues.

Helping organizations build a professional and on-brand customer support workflow that grows with the business.

Making Service Desk an integral part of the GitLab support workflow by providing the tools our teams need.

Helping managers and support ops automate repetitive tasks for their support staff.

Increase awareness of the capabilities of GitLab Service Desk and how it can help our customers handle customer support.

### Recent updates

Please feel free to [subscribe to this GitLab issue to receive notifications](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues/3) when new updates are available.

**Watch the latest update video:**

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/AmKy4IGmevc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

#### Update 1

- Video [https://www.youtube.com/watch?v=AmKy4IGmevc]()
- Issue [https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues/4]()

We are also [exploring ideas, existing issues and user feedback](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues/2). Please feel free to contribute.

### Background

We have a [Service Desk offering](https://about.gitlab.com/direction/plan/certify/#service-desk) in GitLab that we'd like to make an integral part of the GitLab support workflow.  We have early usage, and a community of prolific contributors, however internally this work has been deprioritized. and this is a great opportunity for the Incubation Engineering department to make the next stage of development that meets customer needs.
