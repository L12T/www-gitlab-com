---
title: "GitLab's Functional Group Updates: February 1st - 16th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working from February 1st - 16th"
canonical_path: "/blog/2018/02/16/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Support Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1fccSNBOFCUCatckO4zBevnApHNsk2OTO8QPSgeIGI_4)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CCvmGUpqTtk" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/1aOas1wBOmgSqUV36bc6jJ1Vn40QZrzdzWK4IJPXNmlE/edit#slide=id.g1f84ee8ce6_0_41)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/WAJn2h6M_fk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/presentation/d/1zcKJzHOL3i-AywhHB132w4Jl9NT6-645tpqFIgaBNAM/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BNwx-QslzjI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/1Z0RuvmNsjL3dJwMTXBAPziurPLTyYPrOuAx9_5OGbwA/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qbdvWzLY1Ww" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1Ij2MvbCC9eoEm0jp3Cv192lLDD57XESDa_USAnhkJ6I/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/W0dihB1Qh7w" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](https://docs.google.com/presentation/d/1uCyGlUF_rDKvN1ZqENDrpuZFCxYjh1fLp3Fso1Y7S4A/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8EhG1cceSCs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/backend-discussion/2018-02-15/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/JIz6VnJosnE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Customer Success Team

[Presentation slides](https://docs.google.com/presentation/d/1lOIZmjF277uKYcb-k3l21tjMkeCnY4hYEOJliS40Rew/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NyQH_X3LsCk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
